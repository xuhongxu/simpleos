package net.simpleos.backend.menu;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import net.simpleframework.ado.db.IQueryEntitySet;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.menu.AbstractMenuHandle;
import net.simpleframework.web.page.component.ui.menu.MenuBean;
import net.simpleframework.web.page.component.ui.menu.MenuItem;
import net.simpleos.SimpleosUtil;
import net.simpleos.backend.permission.PermissionSession;
import net.simpleos.backend.permission.PlatformUtis;

/**
 * 菜单的组成方式
 * 
 * @author yanfei.li
 * @email eliyanfei@126.com
 * 2013-12-2下午02:38:26
 */
public class MenuNavHandle extends AbstractMenuHandle {

	@Override
	public Collection<MenuItem> getMenuItems(ComponentParameter compParameter, MenuItem menuItem) {
		if (menuItem != null) {
			return null;
		}
		PlatformUtis.initUserMenu(compParameter.getSession().getId(), SimpleosUtil.getLoginUser(compParameter), false);
		final Collection<MenuItem> menuList = new ArrayList<MenuItem>();
		final MenuBean menuNBean = (MenuBean) compParameter.componentBean;

		PermissionSession permissionSession = PlatformUtis.DEFAULT_MENUS.get(compParameter.getSession().getId());
		if (permissionSession == null) {
			permissionSession = new PermissionSession();
		}
		final Set<String> menuSet = new HashSet<String>(permissionSession.menuMap.keySet());
		buildMenu(menuList, menuNBean, 0, menuSet);
		return menuList;
	}

	private void buildMenu(final Collection<MenuItem> menuList, final MenuBean menuBean, Object parentId, final Set<String> menuSet) {
		final IQueryEntitySet<MenuNavBean> qs = MenuNavUtils.appModule.queryBean("mark=0 and parentid=" + parentId + " order by oorder",
				MenuNavBean.class);
		MenuNavBean menu = null;
		while ((menu = qs.next()) != null) {
			MenuItem item = new MenuItem(menuBean);
			if (!menuSet.contains(menu.getName()) && PlatformUtis.hasPermission) {
				continue;
			}
			item.setTitle(menu.getText());
			if (menu.getUrl() != null) {
				final StringBuffer url = new StringBuffer();
				if (!menu.getUrl().startsWith("/") && !menu.getUrl().startsWith("http")) {
					url.append("/");
				}
				if (!menu.isBuildIn()) {
					if (!menu.getUrl().startsWith("http")) {
						url.append("/c");
					}
				}
				url.append(menu.getUrl());
				item.setUrl(url.toString());
			}
			menuList.add(item);
			buildMenu(item.getChildren(), menuBean, menu.getId(), menuSet);
		}
	}
}
